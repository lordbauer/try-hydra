{-# LANGUAGE DataKinds           #-} 
{-# LANGUAGE TemplateHaskell     #-} 
{-# LANGUAGE TypeApplications    #-}

module Policy (mkPolicy, policyScript, compiledCode) where


import qualified PlutusTx                         (CompiledCode, compile, unsafeFromBuiltinData)
import           PlutusTx.Prelude          hiding (Semigroup(..), unless)
import           PlutusLedgerApi.V2             (ScriptContext(..), SerialisedScript, serialiseCompiledCode)

{-# INLINABLE mkPolicy #-}
mkPolicy :: () -> ScriptContext -> Bool
mkPolicy _ _ = True


{-# INLINEABLE untypedPolicy #-}
untypedPolicy :: BuiltinData -> BuiltinData -> ()
untypedPolicy redeemer ctx =
  check
    ( mkPolicy 
        (PlutusTx.unsafeFromBuiltinData redeemer)
        (PlutusTx.unsafeFromBuiltinData ctx)
    )

compiledCode :: PlutusTx.CompiledCode (BuiltinData -> BuiltinData -> ())
compiledCode = 
    $$(PlutusTx.compile [||untypedPolicy||])

policyScript :: SerialisedScript
policyScript = serialiseCompiledCode compiledCode


