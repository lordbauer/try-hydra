{-# LANGUAGE GADTs #-}




module Utils.Address (tryReadAddress) where

import           Cardano.Api                 (deserialiseAddress, AsType( AsAddressAny ), SlotNo(..))
import           Cardano.Api.Shelley         (Address (..), AddressAny(..))
import           Cardano.Crypto.Hash.Class   (hashToBytes)
import           Cardano.Ledger.Credential   (StakeReference(..), Ptr(..), Credential( KeyHashObj,ScriptHashObj ))
import           Cardano.Ledger.Crypto       (StandardCrypto)
import           Cardano.Ledger.Hashes       (ScriptHash (..))
import           Cardano.Ledger.Keys         (KeyHash (..))
import           Data.Text                   (pack)
import           PlutusLedgerApi.V1.Credential (Credential(..), StakingCredential(..))
import           PlutusLedgerApi.V1.Crypto     ()
import           PlutusTx.Builtins           (toBuiltin)
import           Cardano.Ledger.BaseTypes    (CertIx(..), TxIx(..))
import           Prelude as Haskell          (String, Maybe(..), fromIntegral, ($))
import           PlutusLedgerApi.V2        (Address(..), PubKeyHash(..), ScriptHash(..))



credentialLedgerToPlutus :: Cardano.Ledger.Credential.Credential a StandardCrypto -> PlutusLedgerApi.V1.Credential.Credential
credentialLedgerToPlutus (ScriptHashObj (Cardano.Ledger.Hashes.ScriptHash h)) = ScriptCredential $ PlutusLedgerApi.V2.ScriptHash $ toBuiltin $ hashToBytes h
credentialLedgerToPlutus (KeyHashObj (KeyHash h))       = PubKeyCredential $ PubKeyHash $ toBuiltin $ hashToBytes h

stakeReferenceLedgerToPlutus :: StakeReference StandardCrypto -> Maybe StakingCredential
stakeReferenceLedgerToPlutus (StakeRefBase x)                   = Just $ StakingHash $ credentialLedgerToPlutus x
stakeReferenceLedgerToPlutus (StakeRefPtr (Ptr (SlotNo x) (Cardano.Ledger.BaseTypes.TxIx y) (CertIx z))) = Just $ StakingPtr (Haskell.fromIntegral x) (Haskell.fromIntegral y) (Haskell.fromIntegral z)
stakeReferenceLedgerToPlutus StakeRefNull                       = Nothing

tryReadAddress :: Haskell.String -> Maybe PlutusLedgerApi.V2.Address
tryReadAddress x = case deserialiseAddress AsAddressAny $ pack x of
    Nothing                                      -> Nothing
    Just (AddressByron _)                        -> Nothing
    Just (AddressShelley (ShelleyAddress _ p s)) -> Just Address
        { addressCredential        = credentialLedgerToPlutus p
        , addressStakingCredential = stakeReferenceLedgerToPlutus s
        }