{-# LANGUAGE TypeApplications #-}

module Utils.ToJSON (writeJSON, writeOnlyJSON) where 

import           Prelude as Haskell              (FilePath, IO, (.), Maybe(..))
import           Data.ByteString.Lazy as BSL     (writeFile)
import           Data.Aeson                      (encode)
--import           Cardano.Api                     (ScriptDataJsonSchema (ScriptDataJsonDetailedSchema),
--                                                  scriptDataToJson, ScriptRedeemer, ToScriptData, toScriptData)
import           PlutusLedgerApi.V2              (SerialisedScript, ToData, toData)
import           Cardano.Api                        (textEnvelopeToJSON, TextEnvelopeDescr, scriptDataToJson, 
                                                    ScriptDataJsonSchema( ScriptDataJsonDetailedSchema ))
import           Cardano.Api.Shelley                (PlutusScript(PlutusScriptSerialised), PlutusScriptV2, fromPlutusData, unsafeHashableScriptData)
import           Data.ByteString.Short              (toShort)
import           Data.ByteString.Lazy               (ByteString, toStrict)
import           Codec.Serialise                    (serialise)
import           Hydra.Cardano.Api (ScriptDatum(..))

writeJSON :: ToData a => Haskell.FilePath -> a -> Haskell.IO ()
writeJSON file = BSL.writeFile file . encode . scriptDataToJson ScriptDataJsonDetailedSchema . unsafeHashableScriptData . fromPlutusData . toData 

contractDescription :: TextEnvelopeDescr
contractDescription  = "..............."

writeOnlyJSON ::  SerialisedScript -> ByteString
writeOnlyJSON = textEnvelopeToJSON @(PlutusScript PlutusScriptV2) (Just contractDescription) . PlutusScriptSerialised . toShort . toStrict . serialise