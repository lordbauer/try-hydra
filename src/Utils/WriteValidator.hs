{-# LANGUAGE TypeApplications #-}

module Utils.WriteValidator (writeCBORValidator, writeValidatorScript, writeMintingScript) where 

import           Prelude as Haskell (FilePath, IO, Either(..), Maybe(..), (.), writeFile, Char, ($), Bool(..),
                                    print, show, putStrLn, (++), (<>), Show, fst, snd, (==))
import           Cardano.Api (writeFileTextEnvelope, TextEnvelopeDescr, serialiseToTextEnvelope, IsPlutusScriptLanguage, 
                             ToJSON(..), FromJSON(..), PlutusScript(..), PlutusScriptV2, FileError(..), displayError)
import           PlutusLedgerApi.V2 (SerialisedScript, Data(..), serialiseCompiledCode)
import           Cardano.Api.Shelley (PlutusScript (PlutusScriptSerialised))
import           Data.ByteString.Short (toShort)
import           Data.ByteString.Lazy                      (toStrict)
import           Data.List                                 (break, intercalate, reverse)
import           Data.List.Split                           (chunksOf)
import           Codec.Serialise                           (serialise)
import           PlutusTx                                  (CompiledCode, BuiltinData)
import           PlutusTx.Code                             (getPlc, sizePlc)
import           PlutusCore.Pretty                         (prettyPlcReadableDef)
import qualified Data.ByteString.Lazy                      as LBS
import           System.Directory                          (createDirectoryIfMissing)
import           System.FilePath.Posix                     ((<.>), (</>))
import qualified Data.Aeson as Aeson
import           Data.String (fromString)
import qualified Data.ByteString.Short as SBS


-- ##############  Utils ############# --
import           Utils.ToJSON                              (writeOnlyJSON)

format :: Show a => a -> [Char]
format x = h++t
    where
        sp = break (== '.') $ show x
        h = reverse (intercalate "," $ chunksOf 3 $ reverse $ fst sp)
        t = snd sp

contractDescription :: TextEnvelopeDescr
contractDescription  = "..............."

--writeCBORValidator1 :: FilePath -> SerialisedScript -> IO (Either (FileError ()) ())
--writeCBORValidator1 file = writeFileTextEnvelope @(PlutusScript PlutusScriptV2) file (Just contractDescription) . PlutusScriptSerialised . toShort . toStrict . serialise

--instance IsPlutusScriptLanguage lang => ToJSON (PlutusScript lang) where
--  toJSON = Aeson.toJSON . serialiseToTextEnvelope Nothing

fromPlutusScript :: SerialisedScript -> PlutusScript lang
fromPlutusScript =
  PlutusScriptSerialised

writeCBORValidator :: FilePath -> SerialisedScript -> IO ()
writeCBORValidator file item = do 
                          let itemFile = file
                              serialised = Aeson.encode $ serialiseToTextEnvelope (Just $ fromString "al") $ fromPlutusScript @PlutusScriptV2 item
                          LBS.writeFile itemFile serialised


--writeScripts :: [(SerialisedScript, String)] -> IO ()
--  writeScripts plutus =
--    forM_ plutus $ \(item, itemName) -> do
--      let itemFile = itemName <> ".plutus"
--          serialised =
--            Aeson.encode $
--              serialiseToTextEnvelope (Just $ fromString itemName) $
--                fromPlutusScript @PlutusScriptV2 item
--      BL.writeFile itemFile serialised
--      putTextLn $ "  " <> pack itemFile <> ":     " <> sizeInKb (serialise item)

writeValidatorScript  :: CompiledCode (BuiltinData -> BuiltinData -> BuiltinData -> ()) -> [Data] -> [Char] -> IO ()
writeValidatorScript  cC _ contractName = do
    let conDir          = "../dist/build/contracts"
        coresDir        = "../dist/data/core"
        contractScript       = serialiseCompiledCode cC
        plc             = getPlc cC
        plcSize         = sizePlc cC

    createDirectoryIfMissing True conDir
    createDirectoryIfMissing True $ coresDir </> contractName
    writeCBORValidator (conDir </> contractName <.> ".plutus") contractScript
    {-case result of
        Left err -> print $ displayError err
        Right () -> do
                        Haskell.writeFile  (coresDir </> contractName </> "prettifiedTypedPlutusCore" <.> "txt") (show $ prettyPlcReadableDef plc)
                        Haskell.writeFile  (coresDir </> contractName </> "rawTypePlutusCore" <.> "txt") (show plc)
                        Haskell.writeFile  (coresDir </> contractName </> "rawUntypedPlutusCore" <.> "txt") (show contractScript)

                        Haskell.putStrLn        "\n<----------------------------------------CONTRACT INFO----------------------------------------->\n"
                        Haskell.putStrLn    $   "       Name:                       "   ++  contractName
                        Haskell.putStrLn    $   "       Location:                   "   ++  conDir      </> contractName
                        Haskell.putStrLn    $   "       Cores Location:             "   ++  coresDir    </> contractName
                        Haskell.putStrLn    $   "       PLC Size (Bytes):           "   <>  show (format plcSize)
                        Haskell.putStrLn    $   "       CBOR Size (Bytes, max 16,384):          "   <>  show (format $  LBS.length $ writeOnlyJSON contractScript)
                        Haskell.putStrLn        "\n<---------------------------------------------------------------------------------------------->\n"
-}

writeMintingScript  :: CompiledCode (BuiltinData -> BuiltinData -> ()) -> [Data] -> [Char] -> IO ()
writeMintingScript  cC _ contractName = do
    let conDir          = "../dist/build/contracts"
        coresDir        = "../dist/data/core"
        contractScript       =  serialiseCompiledCode cC
        plc             = getPlc cC
        plcSize         = sizePlc cC
    createDirectoryIfMissing True conDir
    createDirectoryIfMissing True $ coresDir </> contractName
    writeCBORValidator (conDir </> contractName <.> ".plutus") contractScript
    {-case result of
        Left err -> print $ displayError err
        Right () -> do
                        Haskell.writeFile  (coresDir </> contractName </> "prettifiedTypedPlutusCore" <.> "txt") (show $ prettyPlcReadableDef plc)
                        Haskell.writeFile  (coresDir </> contractName </> "rawTypePlutusCore" <.> "txt") (show plc)
                        Haskell.writeFile  (coresDir </> contractName </> "rawUntypedPlutusCore" <.> "txt") (show contractScript)

                        Haskell.putStrLn        "\n<----------------------------------------Policy INFO------------------------------------------->\n"
                        Haskell.putStrLn    $   "       Name:                       "   ++  contractName
                        Haskell.putStrLn    $   "       Location:                   "   ++  conDir      </> contractName
                        Haskell.putStrLn    $   "       Cores Location:             "   ++  coresDir    </> contractName
                        Haskell.putStrLn    $   "       PLC Size (Bytes):           "   <>  show (format plcSize)
                        Haskell.putStrLn    $   "       CBOR Size (Bytes):          "   <>  show (format $  LBS.length $ writeOnlyJSON contractScript)
                        Haskell.putStrLn        "\n<---------------------------------------------------------------------------------------------->\n"
-}
