{-# LANGUAGE DataKinds           #-} 
{-# LANGUAGE TemplateHaskell     #-} 

module Always (mkValidator, validatorScript, compiledCode) where


import qualified PlutusTx                         (CompiledCode, compile, unsafeFromBuiltinData)
import           PlutusTx.Prelude          hiding (Semigroup(..), unless)
import           PlutusLedgerApi.V2             (ScriptContext(..), SerialisedScript, serialiseCompiledCode)


{-# INLINABLE mkValidator #-}
mkValidator ::  () -> () -> ScriptContext -> Bool
mkValidator _ _ _ = True


{-# INLINEABLE untypedValidator #-}
untypedValidator :: BuiltinData -> BuiltinData -> BuiltinData -> ()
untypedValidator redeemer dat ctx =
  check
    ( mkValidator
        (PlutusTx.unsafeFromBuiltinData redeemer)
        (PlutusTx.unsafeFromBuiltinData dat)
        (PlutusTx.unsafeFromBuiltinData ctx)
    )

compiledCode :: PlutusTx.CompiledCode (BuiltinData -> BuiltinData -> BuiltinData -> ())
compiledCode = 
    $$(PlutusTx.compile [||untypedValidator||])

validatorScript :: SerialisedScript
validatorScript = serialiseCompiledCode compiledCode


