{-# LANGUAGE OverloadedStrings #-}

import Data.String                             (fromString)
import Prelude                                 (Maybe(..), IO(..), head, drop, read, (++), show)
import System.Environment                      (getArgs)
import PlutusLedgerApi.V1                    (Data (B, Constr, I, List, Map), toData, 
                                                TokenName (..), BuiltinByteString, TxOutRef(..))
import Text.Printf                             (printf)

import Utils.WriteValidator                    (writeMintingScript, writeValidatorScript)
import Utils.Address                           (tryReadAddress)

import Policy as Policy (compiledCode)
import Always as Always (compiledCode)


main :: IO () 
main = do 
        writeMintingScript Policy.compiledCode [] "policy"
        writeValidatorScript Always.compiledCode [] "always"
   