{-# LANGUAGE TypeApplications #-}

import Data.String           (fromString)
import Prelude               (IO(..), Bool(..), head, ($), (++), drop, show, putStrLn, IO(..), FilePath, (.))
import           Data.Aeson                      (encode)
import System.Environment    (getArgs)
import System.Directory      (createDirectoryIfMissing)
import System.FilePath.Posix ((</>))
import Text.Printf           (printf)
import PlutusLedgerApi.V1  (BuiltinByteString, unsafeFromBuiltinData)
import PlutusLedgerApi.V2 (ToData, toData, toBuiltinData)
import           Data.ByteString.Lazy as BSL     (writeFile)
import Hydra.Cardano.Api (PlutusScriptV2, mkScriptDatum, fromPlutusScript, mkScriptWitness, toScriptData)
import Hydra.Cardano.Api


import Always as Always

import Cardano.Ledger.Alonzo.Language (Language (PlutusV1, PlutusV2))

import Utils.ToJSON          (writeJSON)


main :: IO () 
main = do 
    let redDir = "../dist/data/redeemers/"
    createDirectoryIfMissing True redDir
    printf $ "wrote to file: " ++ (redDir ++ "unit.json")
    writeJSON (redDir </> "unit" ++ ".json") ()
