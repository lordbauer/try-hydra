# setup
1. got to `/scripts`
2. run `chmod +x 000-setup.sh` followed by `./000-setup.sh`
    - downloads [private-testnet repository](https://github.com/woofpool/cardano-private-testnet-setup.git) and checkout tag `babbage`
    - downloads [hydra repository](https://github.com/input-output-hk/hydra) and checkout tag `0.11.0`
3. navigate into the hydra dir `cd ../hydra` and run `nix develop`
4. build hydra repository with `cabal build hydra-node` and `cabal build hydra-tools`
5. change `$REPO_HOME` in `env.sh` to your path to this repository
6. replace all `node.socket` with `node.socket` in `/cardano-private-testnet-setup`

# hydra documentation 
1. open a new terminal and enter a nix-shell with `nix develop` in `/hydra`
2. navigate to `cd /docs`
3. execute `yarn`
    - builds documentation
4. execute `yarn start`
    - starts documentation server at http://localhost:3000/head-protocol/

# start private testnet
1. open a new terminal and enter a nix-shell with `nix develop` in `/hydra`
2. start private testnet in `/scripts` with `./001-start-private-testnet.sh`


# run examples
*shutdown and start testnet again after each example*

## bob to alice 
1. open a new terminal and enter a nix-shell with `nix develop` in `/hydra`
2. navigate to `cd ../scripts`
3. execute `./002a-init-bob-to-alice.sh`
    - creates two wallets alice and bob
    - funds them
    - publish hydra scripts
    - creates hydra keys for alice and bob
4. execute `./003a-run-node-alice.sh`
    - starts hydra node for alice
5. open a new terminal and enter a nix-shell with `nix develop` in `/hydra`
6. nagivate to `cd ../scripts`
7. execute `./003a-run-node-bob.sh`
    - starts hydra node for alice
8. open a new terminal and enter a nix-shell with `nix develop` in `/hydra`
9. navigate to `cd ../scripts`
10. execute `./004a-connect-alice.sh`
    - connects to the websocket from the hydra node of alice
11. open a new terminal and enter a nix-shell with `nix develop` in `/hydra`
12. navigate to `cd ../scripts`
13. execute `./004a-connect-bob.sh`
    - connects to websocket from hydra node of bob
14. type `{"tag":"Init", "contestationPeriod": 120}` in one of the two websocket connections
14. open a new terminal and enter a nix-shell with `nix develop` in `/hydra`
15. navigate to `cd ../scripts`
16. execute `./005a-commit-alice.sh`
    - builds commit transaction for alice 
17. copy output json into alices websocket connection 
    - locks funds for committing to the head
16. execute `./005a-commit-bob.sh`
    - builds commit transaction for bob 
17. copy outputed json into alices websocket connection
    - locks funds for committing to the head
*hydra head is now open*
18. type `{"tag":"GetUTxO"}` in one of the two websocket connections
    - queries all transactions in the head
19. execute `./006a-bob-to-alice.sh`
    - builds transaction to send half of bobs funds to alice
20. copy output json into bobs websocket connection
    - sends half of bobs funds to alice in the hydra-head
21. type `{"tag":"Close"}` in one of the two websocket connections
    - close head 
*wait for ContestationPeriod around 120 seconds*
22. type `{"tag":"Fanout"}` in one of the two websocket connections
    - distributes all funds
23. execute `./balance-all.sh`
    - query balance from alice and bob

## token bob to alice 
1. open a new terminal and enter a nix-shell with `nix develop` in `/hydra`
2. nagivate to `cd ../scripts`
3. execute `./002b-init-bob-to-alice-token.sh`
    - creates two wallets alice and bob
    - funds them
    - publish hydra scripts
    - creates hydra keys for alice and bob
    - mint token for bob
4. execute `./003a-run-node-alice.sh`
    - starts hydra node for alice
5. open a new terminal and enter a nix-shell with `nix develop` in `/hydra`
6. navigate to `cd ../scripts`
7. execute `./003a-run-node-bob.sh`
    - starts hydra node for alice
8. open a new terminal and enter a nix-shell with `nix develop` in `/hydra`
9. navigate to `cd ../scripts`
10. execute `./004a-connect-alice.sh`
    - connects to websocket from the hydra node of alice
11. open a new terminal and enter a nix-shell with `nix develop` in `/hydra`
12. navigate to `cd ../scripts`
13. execute `./004a-connect-bob.sh`
    - connects to websocket from hydra node of bob
14. type `{"tag":"Init", "contestationPeriod": 120}` in one of the two websocket connections
14. open a new terminal and enter a nix-shell with `nix develop` in `/hydra`
15. navigate to `cd ../scripts`
16. execute `./005a-commit-alice.sh`
    - builds commit transaction for alice 
17. copy output json into alices websocket connection 
    - locks funds for committing to the head
16. execute `005d-commit-bob-token.sh`
    - builds commit transaction for bob with token
17. copy output json into alices websocket connection
    - locks funds for committing to the head
*hydra head is now open*
18. type `{"tag":"GetUTxO"}` in one of the two websocket connections
    - queries all transactions in the head
19. execute `006b-bob-to-alice-token.sh`
    - builds transaction to send half of bobs funds + token to alice
20. copy output json into bobs websocket connection
    - sends half of bobs funds to alice in the hydra head
21. type `{"tag":"Close"}` in one of the two websocket connections
    - close head 
*wait for ContestationPeriod around 120 seconds*
22. type `{"tag":"Fanout"}` in one of the two websocket connections
    - distributes all funds
23. execute `./balance-all.sh`
    - query balance from alice and bob

## bob to contract, contract to alice 
1. open a new terminal and enter a nix-shell with `nix develop` in `/hydra`
2. navigate to `cd ../scripts`
3. execute `./002a-init-bob-to-alice.sh`
    - creates two wallets alice and bob
    - funds them
    - publish hydra scripts
    - creates hydra keys for alice and bob
4. execute `./003a-run-node-alice.sh`
    - starts hydra node for alice
5. open a new terminal and enter a nix-shell with `nix develop` in `/hydra`
6. navigate to `cd ../scripts`
7. execute `./003a-run-node-bob.sh`
    - starts hydra node for alice
8. open a new terminal and enter a nix-shell with `nix develop` in `/hydra`
9. navigate to `cd ../scripts`
10. execute `./004a-connect-alice.sh`
    - connects to websocket from the hydra node of alice
11. open a new terminal and enter a nix-shell with `nix develop` in `/hydra`
12. navigate to `cd ../scripts`
13. execute `./004a-connect-bob.sh`
    - connects to websocket from hydra node of bob
14. type `{"tag":"Init", "contestationPeriod": 120}` in one of the two websocket connections
14. open a new terminal and enter a nix-shell with `nix develop` in `/hydra`
15. navigate to `cd ../scripts`
16. execute `./005a-commit-alice.sh`
    - builds commit transaction for alice 
17. copy output json into alices websocket connection 
    - locks funds for committing to the head
16. execute `./005a-commit-bob.sh`
    - builds commit transaction for bob 
17. copy output json into alices websocket connection
    - locks funds for committing to the head
*hydra head is now open*
18. type `{"tag":"GetUTxO"}` in one of the two websocket connections
    - queries all transactions in the head
19. execute `./006c-bob-to-contract.sh`
    - builds two lock transactions at always succeed validator
20. copy output json into bobs websocket connection
    - sends two transactions to always succeed validator
21. type `{"tag":"GetUTxO"}` in one of the two websocket connections
    - queries all transactions in the head
22. execute `./007-unlock-alice.sh`
    - builds unlock transaction
    - submits to one of the hydra nodes
23. you now can see there is a new transaction/snapshot at the websocket connections
24. type `{"tag":"Close"}` in one of the two websocket connections
    - close head 
*wait for ContestationPeriod around 120 seconds*
25. type `{"tag":"Fanout"}` in one of the two websocket connections
    - distributes all funds
26. execute `./balance-all.sh`
    - query balance from alice and bob
27. use `cardano-cli query utxo --testnet-magic 42 --address $(cat ../private/always.addr)` to query the balance of the always validator
    - maybe you first need to run `. ./env.sh` to add the correct node socket path

## commit external user 
1. open a new terminal and enter a nix-shell with `nix develop` in `/hydra`
2. navigate to `cd ../scripts`
3. execute `./002c-init-extern-users.sh`
    - creates two wallets alice and bob
    - funds them
    - publish hydra scripts
    - creates hydra keys for alice and bob
4. execute `./003a-run-node-alice.sh`
    - starts hydra node for alice
5. open a new terminal and enter a nix-shell with `nix develop` in `/hydra`
6. navigate to `cd ../scripts`
7. execute `./003a-run-node-bob.sh`
    - starts hydra node for alice
8. open a new terminal and enter a nix-shell with `nix develop` in `/hydra`
9. navigate to `cd ../scripts`
10. execute `./004a-connect-alice.sh`
    - connects to websocket from the hydra node of alice
11. open a new terminal and enter a nix-shell with `nix develop` in `/hydra`
12. navigate to `cd ../scripts`
13. execute `./004a-connect-bob.sh`
    - connects to websocket from hydra node of bob
14. type `{"tag":"Init", "contestationPeriod": 120}` in one of the two websocket connections
14. open a new terminal and enter a nix-shell with `nix develop` in `/hydra`
15. navigate to `cd ../scripts`
16. execute `./005c-commit-user.sh`
    - builds commit transaction for alice 
17. copy output json into alices websocket connection 
    - locks funds for committing to the head
16. execute `./005b-commit-bob.sh`
    - builds commit transaction for bob 
17. copy output json into alices websocket connection
    - locks funds for committing to the head
*hydra head is now open*
18. type `{"tag":"GetUTxO"}` in one of the two websocket connections
    - queries all transactions in the head
19. execute `./006d-bob-to-user.sh`
    - builds transaction to send half of bobs funds to alice
20. copy output json into bobs websocket connection
    - sends half of bobs funds to alice in the hydra-head
21. type `{"tag":"Close"}` in one of the two websocket connections
    - close head 
*wait for ContestationPeriod around 60 seconds*
22. type `{"tag":"Fanout"}` in one of the two websocket connections
    - distributes all funds
23. execute `./balance-all.sh`
    - query balance from alice and bob
24. use `cardano-cli query utxo --testnet-magic 42 --address $(cat ../private/User/User1Cardano.addr)` to query the balance of the always validator
    - maybe you first need to run `. ./env.sh` to add the correct node socket path