#!/bin/bash
# sample input: ./balance-alice.sh

. ./env.sh

cardano-cli query utxo --address $(cat $ALICE_PATH/AliceCardano.addr) --testnet-magic ${TESTNET_MAGIC}
alice_addr=$(cat $ALICE_PATH/AliceCardano.addr)

utxo=$(get_address_smallest_lovelace $alice_addr)
lovelace_am=$(get_UTxO_lovelace_amount $alice_addr $utxo)

echo "{\"tag\": \"Commit\",\"utxo\": {\"$utxo\": {\"address\": \"$alice_addr\",\"value\": {\"lovelace\": $lovelace_am}}}}" > $ALICE_PATH/CommitAlice.json

cat $ALICE_PATH/CommitAlice.json