#!/bin/bash
# sample input: ./000a-start-private-testnet.sh

. ./env.sh

if [[ -d "$NETWORK_DIR_PATH" ]]; then
        rm -r "$NETWORK_DIR_PATH"
    fi

    mkdir $NETWORK_DIR_PATH
    mkdir $TX_PATH
    mkdir $WALLET_PATH
    mkdir $TOKEN_PATH

cd $REPO_HOME/cardano-private-testnet-setup
./scripts/automate.sh