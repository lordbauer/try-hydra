#!/bin/bash
# sample input: ./bob-to-alice.sh
. ./env.sh

alice_addr=$(cat $ALICE_PATH/AliceCardano.addr)
bob_addr=$(cat $BOB_PATH/BobCardano.addr)

contract_addr=$(cat ../private/always.addr)

h=$(cat curBalance | jq 'to_entries | .[] | select(.key == "utxo") | .value | to_entries')

utxo_in_c=$(echo $h | jq --arg addr "$contract_addr" '.[] | select(.value.address == $addr)')
utxo_in_u=$(echo $h | jq --arg addr "$alice_addr" '.[] | select(.value.address == $addr)')


echo "utxoin: $utxo_in_c" 
utxo_in_c=$(echo $utxo_in_c | jq 'select(.value.value.lovelace == 40000000)')
echo "utxoin: $utxo_in" 

utxo_hash_c=$(echo $utxo_in_c | jq -r '.key')
utxo_hash_u=$(echo $utxo_in_u | jq -r '.key')

am_c=$(echo $utxo_in_c | jq '.value.value.lovelace')
am_u=$(echo $utxo_in_u | jq '.value.value.lovelace')
#am_c=$(expr $am_c - 1528800)
echo $utxo_hash_c
echo $am_c

echo $utxo_hash_u
echo $am_u

cardano-cli transaction build-raw \
    --tx-in-collateral $utxo_hash_u \
    --tx-in $utxo_hash_c  \
    --tx-in-script-file ../dist/build/contracts/always.plutus \
    --tx-in-inline-datum-present \
    --tx-in-redeemer-file ../dist/data/redeemers/unit.json \
    --tx-in-execution-units="(10000000000, 14000000)" \
    --tx-out $alice_addr+$am_c \
    --protocol-params-file protocol-parameters.json \
    --fee 0 \
    --out-file $TX_PATH/tx.raw

cardano-cli transaction sign --tx-body-file $TX_PATH/tx.raw --signing-key-file $ALICE_PATH/AliceCardano.sk --out-file $TX_PATH/tx.signed

echo "{\"tag\": \"NewTx\",\"transaction\": $(cat $TX_PATH/tx.signed | jq '.cborHex')}" 

echo "{\"tag\": \"NewTx\",\"transaction\": $(cat $TX_PATH/tx.signed | jq '.cborHex')}" | websocat ws://127.0.0.1:4001 |
while read line
do
   echo ""
done

