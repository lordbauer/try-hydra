#!/bin/bash
# sample input: ./bob-to-alice.sh

. ./env.sh
alice_addr=$(cat $ALICE_PATH/AliceCardano.addr)
bob_addr=$(cat $BOB_PATH/BobCardano.addr)

h=$(cat curBalance | jq 'to_entries | .[] | select(.key == "utxo") | .value | to_entries')

utxo_in=$(echo $h | jq --arg addr "$bob_addr" '.[] | select(.value.address == $addr)')


echo "utxoin: $utxo_in" 
utxo_hash=$(echo $utxo_in | jq -r '.key')

am=$(echo $utxo_in | jq '.value.value.lovelace')
half=$(expr $am / 2)

cardano-cli transaction build-raw \
    --tx-in $utxo_hash  \
    --tx-out $bob_addr+$half \
    --tx-out $alice_addr+$half \
    --fee 0 \
    --out-file $TX_PATH/tx.raw

cardano-cli transaction sign --tx-body-file $TX_PATH/tx.raw --signing-key-file $BOB_PATH/BobCardano.sk --out-file $TX_PATH/tx.signed

echo "{\"tag\": \"NewTx\",\"transaction\": $(cat $TX_PATH/tx.signed | jq '.cborHex')}" > $TX_PATH/bob-to-alice.json

cat $TX_PATH/bob-to-alice.json
