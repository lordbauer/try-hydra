#!/bin/bash
# sample input: ./balance-bob.sh

. ./env.sh


user_addr=$(cat $USER_PATH/User1Cardano.addr)
cardano-cli query utxo --address $user_addr --testnet-magic ${TESTNET_MAGIC}
utxo=$(get_address_smallest_lovelace $user_addr)
lovelace_am=$(get_UTxO_lovelace_amount $user_addr $utxo)
echo $utxo
echo "{ \"$utxo\": {\"address\":\"$user_addr\",\"value\": {\"lovelace\": $lovelace_am}}}" | jq
curl http://localhost:4001/commit -d "{ \"$utxo\": {\"address\":\"$user_addr\",\"value\": {\"lovelace\": $lovelace_am}}}" > $TX_PATH/tx.raw

cardano-cli transaction sign --tx-file $TX_PATH/tx.raw --signing-key-file $USER_PATH/User1Cardano.sk --testnet-magic ${TESTNET_MAGIC} --out-file $TX_PATH/tx.signed

cardano-cli transaction submit --tx-file $TX_PATH/tx.signed --testnet-magic ${TESTNET_MAGIC}