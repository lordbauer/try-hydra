#!/bin/bash

##### Network #####

export REPO_HOME="$HOME/gimbalabs/try-hydra" #path to this repository
export CARDANO_NODE_SOCKET_PATH="$REPO_HOME/cardano-private-testnet-setup/private-testnet/node-spo1/node.socket" #path to node socket 
export NETWORK_DIR_PATH="$REPO_HOME/private" # path to network in use (preprod/private)
export TESTNET_MAGIC=42 # magic refeering to network in use (1/42)
export PROTOCOL="$NETWORK_DIR_PATH/protocol.json"
export TX_PATH="$NETWORK_DIR_PATH/tx"

# hydra 
hydra_tools="$REPO_HOME/hydra/dist-newstyle/build/x86_64-linux/ghc-8.10.7/hydra-node-0.11.0/x/hydra-tools/build/hydra-tools/hydra-tools"
hydra_node="$REPO_HOME/hydra/dist-newstyle/build/x86_64-linux/ghc-8.10.7/hydra-node-0.11.0/x/hydra-node/build/hydra-node/hydra-node"


##### Users #####

export WALLET_PATH="$NETWORK_DIR_PATH/wallets"
export ALICE_PATH="$WALLET_PATH/Alice"
export BOB_PATH="$WALLET_PATH/Bob"
export USER_PATH="$WALLET_PATH/User"

##### Tokens #####

export TOKEN_PATH="$NETWORK_DIR_PATH/tokens"


##### Contracts #####

DATUM_PATH="../dist/data/datums"
REDEEMER_PATH="../dist/data/redeemers"
CONTRACTS_FILE_PATH="../dist/build/contracts"

##### Functions #####

export GREEN=$(tput setaf 2)
export BLUE=$(tput setaf 4)
export NORMAL=$(tput sgr0)

printfWithColor() {
    printf "\n${GREEN}$1${NORMAL}\n"
}

echoWithColor() {
    echo "\n${GREEN}$1${NORMAL}\n"
}

sleep2() {
    sleep 2
}

get_address_smallest_lovelace() {
    cardano-cli query utxo --address $1 --testnet-magic ${TESTNET_MAGIC} |
        tail -n +3 |
        awk '{printf "%s#%s %s \n", $1 , $2, $3}' |
        sort -n -k2 |
        head -n1 |
        awk '{print $1}'
}

# $1 is the address
# $2 is the UtxO
get_UTxO_lovelace_amount() {
    for i in {1..10}; do
        utxoAttachedHex=$(
            cardano-cli query utxo --address $1 --testnet-magic ${TESTNET_MAGIC} |
                tail -n +3 |
                awk '{printf "%s#%s %s\n",$1, $2, $3}' |
                sort -gr |
                awk -v i="$i" 'NR == i {printf("%s", $1)}'
        )
        if [ "$utxoAttachedHex" == "$2" ]; then
            cardano-cli query utxo --address $1 --testnet-magic ${TESTNET_MAGIC} |
                tail -n +3 |
                awk '{printf "%s#%s %s\n",$1, $2, $3}' |
                sort -gr |
                awk -v i="$i" 'NR == i {printf("%s", $2)}'
            return
        fi
    done
}

# $1 is address
# $2 is the token
get_token_count() {
    for i in {1..10}; do
        utxoAttachedHex=$(
            cardano-cli query utxo --address $1 --testnet-magic ${TESTNET_MAGIC} |
                tail -n +3 |
                awk '{printf "%s %s#%s %s\n",$6, $1, $2, $7}' |
                sort -gr |
                awk -v i="$i" 'NR == i {printf("%s", $3)}'
        )
        if [ "$utxoAttachedHex" == "$2" ]; then
            cardano-cli query utxo --address $1 --testnet-magic ${TESTNET_MAGIC} |
                tail -n +3 |
                awk '{printf "%s %s#%s %s\n",$6, $1, $2, $7}' |
                sort -gr |
                awk -v i="$i" 'NR == i {printf("%s", $1)}'
            return
        fi
    done
}


get_address_biggest_lovelace() {
    cardano-cli query utxo --address $1 --testnet-magic ${TESTNET_MAGIC} |
        tail -n +3 |
        awk '{printf "%s#%s %s \n", $1 , $2, $3}' |
        sort -rn -k2 |
        head -n1 |
        awk '{print $1}'
}

get_UTxO_by_token() {
    for i in {1..10}; do
        utxoAttachedHex=$(
            cardano-cli query utxo --address $1 --testnet-magic ${TESTNET_MAGIC} |
                tail -n +3 |
                awk '{printf "%s %s#%s %s\n",$6, $1, $2, $7}' |
                sort -gr |
                awk -v i="$i" 'NR == i {printf("%s", $3)}'
        )
        if [ "$utxoAttachedHex" == "$2" ]; then
            cardano-cli query utxo --address $1 --testnet-magic ${TESTNET_MAGIC} |
                tail -n +3 |
                awk '{printf "%s %s#%s\n",$6, $1, $2}' |
                sort -gr |
                awk -v i="$i" 'NR == i {printf("%s", $2)}'
            return
        fi
    done
}