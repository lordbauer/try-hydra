#!/bin/bash
# sample input: ./balance-bob.sh

. ./env.sh

cardano-cli query utxo --address $(cat $BOB_PATH/BobCardano.addr) --testnet-magic ${TESTNET_MAGIC}
bob_addr=$(cat $BOB_PATH/BobCardano.addr)

utxo=$(get_address_smallest_lovelace $bob_addr)
lovelace_am=$(get_UTxO_lovelace_amount $bob_addr $utxo)


echo "{\"tag\": \"Commit\",\"utxo\": {\"$utxo\": {\"address\": \"$bob_addr\",\"value\": {\"lovelace\": $lovelace_am}}}}" > $BOB_PATH/CommitBob.json

cat $BOB_PATH/CommitBob.json 