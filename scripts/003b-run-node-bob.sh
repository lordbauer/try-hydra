#!/bin/bash
# sample input: ./run-node.sh

. ./env.sh

hydra_scripts=$(echo $(cat hydra_scr_tx))

cd $BOB_PATH

$hydra_node \
    --node-id 2 --port 5002 --api-port 4002 \
    --peer 127.0.0.1:5001 \
    --hydra-signing-key BobHydra.sk \
    --hydra-verification-key ../Alice/AliceHydra.vk \
    --hydra-scripts-tx-id $hydra_scripts \
    --cardano-signing-key BobCardano.sk \
    --cardano-verification-key ../Alice/AliceCardano.vk \
    --ledger-protocol-parameters ../../../scripts/protocol-parameters.json \
    --testnet-magic ${TESTNET_MAGIC} \
    --node-socket $CARDANO_NODE_SOCKET_PATH 

