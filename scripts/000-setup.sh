

chmod +x 001-start-private-testnet.sh
chmod +x 002a-init-bob-to-alice.sh
chmod +x 002b-init-bob-to-alice-token.sh
chmod +x 002c-init-extern-users.sh
chmod +x 003a-run-node-alice.sh
chmod +x 003b-run-node-bob.sh
chmod +x 004a-connect-alice.sh
chmod +x 004b-connect-bob.sh
chmod +x 005a-commit-alice.sh
chmod +x 005b-commit-bob.sh
chmod +x 005c-commit-user.sh
chmod +x 005d-commit-bob-token.sh
chmod +x 006a-bob-to-alice.sh
chmod +x 006b-bob-to-alice-token.sh
chmod +x 006c-bob-to-contract.sh
chmod +x 006d-bob-to-user.sh
chmod +x 007-alice-unlock.sh

chmod +x balance-all.sh
chmod +x balance-head.sh


# private testnet
cd ..

git clone https://github.com/woofpool/cardano-private-testnet-setup.git

cd cardano-private-testnet-setup
git checkout babbage


# hydra
cd ..

git clone https://github.com/input-output-hk/hydra

cd hydra
git checkout 0.11.0


