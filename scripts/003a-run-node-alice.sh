#!/bin/bash
# sample input: ./run-node.sh

. ./env.sh

echo h
hydra_scripts=$(echo $(cat hydra_scr_tx))

cd $ALICE_PATH

$hydra_node \
    --node-id 1 --port 5001 --api-port 4001 \
    --peer 127.0.0.1:5002 \
    --hydra-signing-key AliceHydra.sk \
    --hydra-verification-key ../Bob/BobHydra.vk \
    --hydra-scripts-tx-id $hydra_scripts \
    --cardano-signing-key AliceCardano.sk \
    --cardano-verification-key ../Bob/BobCardano.vk \
    --ledger-protocol-parameters ../../../scripts/protocol-parameters.json \
    --testnet-magic ${TESTNET_MAGIC} \
    --node-socket $CARDANO_NODE_SOCKET_PATH 