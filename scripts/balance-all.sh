#!/bin/bash
# sample input: ./hydra-scripts-balance.sh

. ./env.sh

printfWithColor "Alice"
cardano-cli query utxo --testnet-magic ${TESTNET_MAGIC} --address $(cat $ALICE_PATH/AliceCardano.addr)

printfWithColor "Bob"
cardano-cli query utxo --testnet-magic ${TESTNET_MAGIC} --address $(cat $BOB_PATH/BobCardano.addr)

printfWithColor "vHead"
cardano-cli address build --testnet-magic ${TESTNET_MAGIC} --payment-script-file ../hydra/hydra-plutus/scripts/vHead.plutus --out-file ../private/vHead.addr
cardano-cli query utxo --testnet-magic ${TESTNET_MAGIC} --address $(cat ../private/vHead.addr)

printfWithColor "vCommit"
cardano-cli address build --testnet-magic ${TESTNET_MAGIC} --payment-script-file ../hydra/hydra-plutus/scripts/vCommit.plutus --out-file ../private/vCommit.addr
cardano-cli query utxo --testnet-magic ${TESTNET_MAGIC} --address $(cat ../private/vCommit.addr)

printfWithColor "vInitial"
cardano-cli address build --testnet-magic ${TESTNET_MAGIC} --payment-script-file ../hydra/hydra-plutus/scripts/vInitial.plutus --out-file ../private/vInitial.addr
cardano-cli query utxo --testnet-magic ${TESTNET_MAGIC} --address $(cat ../private/vInitial.addr)
