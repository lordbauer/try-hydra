#!/bin/bash
#sample input: ./hydra-demo.sh

. ./env.sh

ALICE_PATH="$WALLET_PATH/Alice"
BOB_PATH="$WALLET_PATH/Bob"
USER_PATH="$WALLET_PATH/User"

mkdir $ALICE_PATH
mkdir $BOB_PATH
mkdir $USER_PATH

printfWithColor "Create keys..."
cardano-cli address key-gen --verification-key-file $ALICE_PATH/AliceCardano.vk --signing-key-file $ALICE_PATH/AliceCardano.sk
cardano-cli address build --payment-verification-key-file $ALICE_PATH/AliceCardano.vk --testnet-magic ${TESTNET_MAGIC} --out-file $ALICE_PATH/AliceCardano.addr

cardano-cli address key-gen --verification-key-file $BOB_PATH/BobCardano.vk --signing-key-file $BOB_PATH/BobCardano.sk
cardano-cli address build --payment-verification-key-file $BOB_PATH/BobCardano.vk --testnet-magic ${TESTNET_MAGIC} --out-file $BOB_PATH/BobCardano.addr

cardano-cli address key-gen --verification-key-file $USER_PATH/User1Cardano.vk --signing-key-file $USER_PATH/User1Cardano.sk
cardano-cli address build --payment-verification-key-file $USER_PATH/User1Cardano.vk --testnet-magic ${TESTNET_MAGIC} --out-file $USER_PATH/User1Cardano.addr


printfWithColor "Fund wallets..."

USER_ADDR=$(cat ../cardano-private-testnet-setup/private-testnet/addresses/payment1.addr)

UTXO_IN=$(get_address_biggest_lovelace ${USER_ADDR})

cardano-cli query protocol-parameters --testnet-magic ${TESTNET_MAGIC} --out-file pp.json

sleep2
sleep2

cardano-cli transaction build \
    --testnet-magic ${TESTNET_MAGIC} \
    --tx-in $UTXO_IN \
    --tx-in-collateral $UTXO_IN \
    --tx-out $(cat $ALICE_PATH/AliceCardano.addr)+100000000 \
    --tx-out $(cat $ALICE_PATH/AliceCardano.addr)+100000000 \
    --tx-out-datum-hash a654fb60d21c1fed48db2c320aa6df9737ec0204c0ba53b9b94a09fb40e757f3 \
    --tx-out $(cat $BOB_PATH/BobCardano.addr)+80000000 \
    --tx-out $(cat $BOB_PATH/BobCardano.addr)+100000000 \
    --tx-out-datum-hash a654fb60d21c1fed48db2c320aa6df9737ec0204c0ba53b9b94a09fb40e757f3 \
    --tx-out $(cat $USER_PATH/User1Cardano.addr)+80000000 \
    --protocol-params-file pp.json \
    --change-address $USER_ADDR \
    --out-file $TX_PATH/fund.raw

cardano-cli transaction sign \
    --tx-body-file $TX_PATH/fund.raw \
    --testnet-magic ${TESTNET_MAGIC} \
    --signing-key-file ../cardano-private-testnet-setup/private-testnet/stake-delegator-keys/payment1.skey \
    --out-file $TX_PATH/fund.signed

cardano-cli transaction submit \
    --tx-file $TX_PATH/fund.signed \
    --testnet-magic ${TESTNET_MAGIC}

sleep2
sleep2


printfWithColor "Publish hydra scripts..."

$hydra_node publish-scripts --testnet-magic ${TESTNET_MAGIC} --node-socket ${CARDANO_NODE_SOCKET_PATH} --cardano-signing-key $ALICE_PATH/AliceCardano.sk > hydra_scr_tx

sleep2
sleep2

$hydra_tools gen-hydra-key --output-file $ALICE_PATH/AliceHydra
$hydra_tools gen-hydra-key --output-file $BOB_PATH/BobHydra

#cp ../hydra/hydra-cluster/config/protocol-parameters.json ../scripts/protocol-parameters.json

printfWithColor "Alice"
cardano-cli query utxo --address $(cat $ALICE_PATH/AliceCardano.addr) --testnet-magic ${TESTNET_MAGIC}

printfWithColor "Bob"
cardano-cli query utxo --address $(cat $BOB_PATH/BobCardano.addr) --testnet-magic ${TESTNET_MAGIC}

printfWithColor "User1"
cardano-cli query utxo --address $(cat $USER_PATH/User1Cardano.addr) --testnet-magic ${TESTNET_MAGIC}



